<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('parametre', function (Blueprint $table) {
            $table->id();
            $table->string('nom_site')->nullable;
            $table->string('image_site')->nullable;
            $table->string('phone')->nullable;
            $table->string('Email')->nullable;
            $table->string('commune')->nullable;
            $table->string('localisation')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('parametre');
    }
};
