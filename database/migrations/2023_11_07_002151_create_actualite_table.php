<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('actualite', function (Blueprint $table) {
            $table->id();
            $table->foreignId('axe_id');
            $table->string('image_actualite');
            $table->string('prof');
            $table->string('titre_actualite');
            $table->string('Lieu')->nullable();
            $table->string('datedebut');
            $table->string('heuredebut');
            $table->string('datefin');
            $table->string('heurefin');
            $table->string('mini_description');
            $table->text('description_actualite');
            $table->string('Home');
            $table->string('calendrier');
            $table->string('Archive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('actualite');
    }
};
