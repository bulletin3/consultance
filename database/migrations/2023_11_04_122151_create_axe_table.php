<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('axe', function (Blueprint $table) {
            $table->id();
            $table->string('nom_axes');
            $table->string('image_axes')->nullable();
            $table->text('description_axes');
            $table->String('isActif');
            //$table->string('Archivre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('axe');
    }
};
