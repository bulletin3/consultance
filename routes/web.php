<?php

use App\Http\Controllers\ActualiteController;
use App\Http\Controllers\AxesController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\GalerieController;
use App\Http\Controllers\ParametreController;
use App\Models\Axes;
use App\Models\Galerie;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

/**
 * Auth Routes
 */
Auth::routes(['verify' => false]);


Route::group(['namespace' => 'App\Http\Controllers'], function()
{
    Route::middleware('auth')->group(function () {
        /**
         * Home Routes
         */
        Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
        /**
         * Role Routes
         */
        Route::resource('roles', RolesController::class);
        /**
         * Permission Routes
         */
        Route::resource('permissions', PermissionsController::class);
        /**
         * User Routes
         */
        Route::group(['prefix' => 'users'], function() {
            Route::get('/', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');
            Route::get('/create', 'UsersController@create')->name('users.create');
            Route::post('/create', 'UsersController@store')->name('users.store');
            Route::get('/{user}/show', 'UsersController@show')->name('users.show');
            Route::get('/{user}/edit', 'UsersController@edit')->name('users.edit');
            Route::patch('/{user}/update', 'UsersController@update')->name('users.update');
            Route::delete('/{user}/delete', 'UsersController@destroy')->name('users.destroy');
        });
    });
});

//Admin
Route::resource('/Axes',  AxesController::class);
Route::resource('/Client',  ClientController::class);
Route::resource('/Parametre',  ParametreController::class);
Route::resource('/actualite',  ActualiteController::class);
Route::resource('/galery',  GalerieController::class);













//User


Route::get('/', [App\Http\Controllers\AxesController::class,'indexuser'])->name('axes.list');
Route::get('/nosDomaines', [App\Http\Controllers\AxesController::class,'nosDomaines'])->name('nosDomaines');
Route::get('/catalogue', [App\Http\Controllers\AxesController::class,'catalogue'])->name('catalogue');
Route::get('/actualites', [App\Http\Controllers\ActualiteController::class,'Actualite'])->name('actualites.index');
Route::get('/actualites/{id}', [ActualiteController::class, 'detail']);
Route::get('/calendriers', [App\Http\Controllers\ActualiteController::class,'calendriers'])->name('calendriers.index');
//Route::get('/', [App\Http\Controllers\ClientController::class,'indexuser'])->name('client.list');
