<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $clients = Client::all();
        return view("Client.list", compact("clients"));
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("Client.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $file = time() .".". $request->file("image_client")->extension();
        $path = $request->file('image_client')->storeAs(
            'image_client',
            $file,
            'public',
        );
        // $request->validate([
        //     'description_axes'=> 'required',
        //     'nom_axes'=> 'required',
        // ]);
        //$request->image_axes = $name;

        //dd($request->all());
        $post = Client::create(
            [
                'isActif_client'=> $request->isActif_client,
                'image_client' => $path,
            ]
        );

        $axes =new Client();
        $axes->path = $path;
        //$post->axes()->save($axes);

        session()->flash('success', 'Axes Creer successfully');
        return redirect()->route('Client.index');

    }

    /**
     * Display the specified resource.
     */
    public function show(Client $client): Response
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Client $client): Response
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Client $client): RedirectResponse
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $Axes = Client::find($id);
        $Axes->delete();
        session()->flash('success','Client supprimer Avec success');
            return redirect()->route('Client.index');
    }
}
