<?php

namespace App\Http\Controllers;

use App\Models\Actualite;
use App\Models\Axes;
use App\Models\Parametre;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Builder;

class ActualiteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $actualite = Actualite::all();
        return view("Actualite.list", compact("actualite"));
    }

    public function Actualite(Request $request)
    {
        $parametre = Parametre::all();
        $axes = Axes::all();
        $actualite = Actualite::query()
            ->when(
                $request->q,
                function (Builder $builder) use ($request) {
                    $builder->where('titre_actualite', 'like', "%{$request->q}%")
                        ->orWhere('prof', 'like', "%{$request->q}%")
                        ->orwhere('axe_id', 'like', "%{$request->qf}%");
                }
            )
            ->simplePaginate(1);
            //dd($actualite);

        return view("pageuser.Actualite.index", compact("actualite", 'parametre','axes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $actualite = Actualite::all();
        $axes = Axes::all();
    return view("Actualite.create", compact("actualite", "axes"));
    }

    public function calendriers()
    {
        $actualite = Actualite::all();
        $axes = Axes::all();
    return view("pageuser.calendrier.index", compact("actualite", "axes"));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $file = time() .".". $request->file("image_actualite")->extension();
        $path = $request->file('image_actualite')->storeAs(
            'image_actualite',
            $file,
            'public',
        );
        // $request->validate([
        //     'description_axes'=> 'required',
        //     'nom_axes'=> 'required',
        // ]);
        //$request->image_axes = $name;

        //dd($request->all());
        $post = Actualite::create(
            [
                'calendrier'=> $request->calendrier,
                'axe_id'=> $request->axe_id,
                'prof'=> $request->prof,
                'titre_actualite'=> $request->titre_actualite,
                'Lieu'=> $request->Lieu,
                'datedebut'=> $request->datedebut,
                'heuredebut'=> $request->heuredebut,
                'datefin'=> $request->datefin,
                'heurefin'=> $request->heurefin,
                'mini_description'=> $request->mini_description,
                'description_actualite'=> $request->description_actualite,
                'Home'=> $request->Home,
                'Archive'=> $request->Archive,
                'image_actualite' => $path,
            ]
        );

        $axes =new Actualite();
        $axes->path = $path;
        //$post->axes()->save($axes);

        session()->flash('success', 'Actualite Creer successfully');
        return redirect()->route('actualite.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Actualite $actualite): Response
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Actualite $actualite): Response
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Actualite $actualite): RedirectResponse
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $Axes = Actualite::find($id);
        $Axes->delete();
        session()->flash('success','Actualite supprimer Avec success');
            return redirect()->route('actualite.index');
    }


    public function detail($id)
    {
        //dd($id);
        $parametre = parametre::all();
        $actualite = Actualite::where('id', $id)->get();
        $axes = Axes::All();


        //dd($essai);
        //dd($actualite);
        return view('pageuser.Actualite.detail', compact('actualite', 'parametre','axes'));
    }
}
