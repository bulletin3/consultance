<?php

namespace App\Http\Controllers;

use App\Models\Parametre;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ParametreController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $parametres = Parametre::all();
        return view("parametre.list", compact("parametres"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("parametre.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $file = time() .".". $request->file("image_site")->extension();
        $path = $request->file('image_site')->storeAs(
            'image_site',
            $file,
            'public',
        );
        // $request->validate([
        //     'description_axes'=> 'required',
        //     'nom_axes'=> 'required',
        // ]);
        //$request->image_axes = $name;

        //dd($request->all());
        $post = Parametre::create(
            [
                'nom_site'=> $request->nom_site,
                'phone'=> $request->phone,
                'Email'=> $request->Email,
                'commune'=> $request->commune,
                'localisation'=> $request->localisation,
                'image_site' => $path,
            ]
        );

        $axes =new Parametre();
        $axes->path = $path;
        //$post->axes()->save($axes);

        session()->flash('success', 'Parametre Creer successfully');
        return redirect()->route('Parametre.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Parametre $parametre): Response
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Parametre $parametre): Response
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Parametre $parametre): RedirectResponse
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $Axes = Parametre::find($id);
        $Axes->delete();
        session()->flash('success','Parametre supprimer Avec success');
            return redirect()->route('Parametre.index');
    }
}
