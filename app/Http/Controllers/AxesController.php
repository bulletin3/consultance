<?php

namespace App\Http\Controllers;

use App\Models\Axes;
use App\Models\Client;
use App\Models\Parametre;
use Database\Seeders\AxesSeeder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class AxesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Axes = Axes::all();
        //dd($Axes);
        return view("Axes.list",compact("Axes"));
    }

    public function indexuser()
    {
        $clients = Client::all();
        $parametre = Parametre::all();
        $Axes = Axes::where("isActif", 'Activer')->get();
        //dd($Axes);
        return view("welcome",compact("Axes","clients", "parametre"));
    }

    public function nosDomaines()
    {
        $Axes = Axes::all();
        $parametre = Parametre::all();
        return view("pageuser.Domaine.index",compact("Axes","parametre"));
    }

    public function catalogue()
    {
        $Axes = Axes::all();
        $parametre = Parametre::all();
        //dd($Axes);
        return view("pageuser.catalogue.index",compact("Axes","parametre"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view("Axes.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        // if ($request->hasFile('photo')) {
        //     $request->file('image_axes');
        // $request->image_axes;
        // }
        //$name =Storage::disk("public")->put("image_axes", $request->file("image_axes"));
        //dd($name);
        $file = time() .".". $request->file("image_axes")->extension();
        $path = $request->file('image_axes')->storeAs(
            'image_axes',
            $file,
            'public',
        );
        // $request->validate([
        //     'description_axes'=> 'required',
        //     'nom_axes'=> 'required',
        // ]);
        //$request->image_axes = $name;

        //dd($request->all());
        $post = Axes::create(
            [
                'isActif'=> $request->isActif,
                'image_axes' => $path,
                'nom_axes' => $request->nom_axes,
            'description_axes'=> $request->description_axes
            ]
        );

        $axes =new Axes();
        $axes->path = $path;
        //$post->axes()->save($axes);

        session()->flash('success', 'Axes Creer successfully');
        return redirect()->route('Axes.index');

            //->withSuccess(__('Axes cree avec success.'));

    }

    /**
     * Display the specified resource.
     */
    public function show(Axes $axes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $Axes = Axes::find($id);
        //dd($niveau);
        return view('Axes.edit',  compact('Axes') );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id): RedirectResponse
    {

        $request->validate(
            $request->all()
        );

        $Axes = Axes::find($id);
        $Axes->update($request->all());

        session()->flash('success', 'Axe Modifie avec success.');

        return redirect()->route('Axes.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $Axes = Axes::find($id);
        $Axes->delete();
        session()->flash('success','Axe supprimer Avec success');
            return redirect()->route('Axes.index');
    }
}
