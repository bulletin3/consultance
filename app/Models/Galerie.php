<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Galerie extends Pivot
{
    use HasFactory;
    protected $table ='galery';
    protected $guarded = [];
    public $timestamps = true;
}
