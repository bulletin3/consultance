<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Axes extends Pivot
{
    use HasFactory;
    protected $table ='axe';
    protected $guarded = [];
    public $timestamps = true;
}
