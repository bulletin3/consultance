<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Parametre extends Pivot
{
    use HasFactory;
    protected $table ='parametre';
    protected $guarded = [];
    public $timestamps = true;
}
