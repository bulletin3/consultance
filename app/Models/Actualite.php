<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Actualite extends Pivot
{
    use HasFactory;
    protected $table ='actualite';
    protected $guarded = [];
    public $timestamps = true;
}
