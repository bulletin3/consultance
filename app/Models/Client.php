<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Client extends Pivot
{
    use HasFactory;
    protected $table ='client';
    protected $guarded = [];
    public $timestamps = true;
}
