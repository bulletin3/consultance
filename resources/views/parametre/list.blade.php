@extends('layouts.app')

@section('title')
Client List
@endsection

@section('content')

<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Client</h5>
            <h6 class="card-subtitle mb-2 text-muted">Gerer vos Client</h6>

            <div class="mt-2">
                @include('layouts.includes.messages')
            </div>
            <div class="mb-2 text-end">

                @if ($parametres->count() !== 0)
                    @else
                    <a href="{{ route('Parametre.create') }}" class="btn btn-info btn-loading" data-coreui-toggle="loading-button ">Ajouter Parametre</a>
                @endif

            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" width="1%">#</tpermissionh>
                        <th scope="col" width="15%">Image</th>
                        <th scope="col" width="15%">Nom Site</th>
                        <th scope="col" width="15%">Phone</th>
                        <th scope="col" width="15%">Email</th>
                        <th scope="col" width="15%">Commune</th>
                        <th scope="col" width="15%">Localisation</th>
                        <th width="3%" colspan="3">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($parametres as $items)
                    <tr>
                        <th scope="row">{{ $items->id }}</th>
                        <td> <img src="{{Storage::url($items->image_site)}}" width="50" height="60"></td>
                        <th scope="row">{{ $items->nom_site }}</th>
                        <th scope="row">{{ $items->phone }}</th>
                        <th scope="row">{{ $items->Email }}</th>
                        <th scope="row">{{ $items->commune }}</th>
                        <th scope="row">{{ $items->localisation }}</th>
                        <td><a href="{{ route('Client.show', $items->id) }}" class="btn btn-warning btn-sm">Show</a></td>
                        <td><a href="{{ route('Client.edit', $items->id) }}" class="btn btn-info btn-sm">Edit</a></td>
                        <td>
                            {!! Form::open(['method' => 'DELETE','route' => ['Client.destroy',
                            $items->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="d-flex">
                {{-- {!! $users->links() !!} --}}
            </div>

        </div>
    </div>
</div>
@endsection
