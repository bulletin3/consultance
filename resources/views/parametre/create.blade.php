@extends('layouts.app')

@section('title')
Create Parametre
@endsection

@section('content')
<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ajouter  parametre</h5>
            <div class="p-4 rounded">
                <div class="container mt-4">



                    <form method="POST" action="{{route('Parametre.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="row">
                              <div class="col">
                                <label class="block mb-4">
                                    <span class="sr-only">Choose File</span>
                                    <input type="file" name="image_site"/>
                                    @error('image')
                                    <span class="text-red-600 text-sm">{{ $message }}</span>
                                    @enderror
                                </label>
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">Nom site</label>
                                    <input value="{{ old('nom_site') }}" type="text" class="form-control" name="nom_site"
                                        >
                                </div>
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">phone</label>
                                    <input value="{{ old('phone') }}" type="text" class="form-control" name="phone"
                                        >
                                </div>
                                <div class="mb-1">
                                    <label for="Email" class="form-label">Email</label>
                                    <input value="{{ old('Email') }}" type="email" class="form-control" name="Email"
                                        >
                                </div>
                                <div class="mb-1">
                                    <label for="commune" class="form-label">commune</label>
                                    <input value="{{ old('commune') }}" type="text" class="form-control" name="commune"
                                        >
                                </div>
                                <div class="mb-1">
                                    <label for="localisation" class="form-label">localisation</label>
                                    <input value="{{ old('localisation') }}" type="text" class="form-control" name="localisation"
                                        >
                                </div>
                              </div>
                            </div>
                          </div>

                        <button type="submit" class="btn btn-primary">Enregistrer Niveau</button>
                        <a href="{{ route('Parametre.index') }}" class="btn btn-default">Back</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endsection

