@extends('layouts.app')

@section('title')
    Modiifier Etudiant
@endsection

@section('content')
    <div class="bg-light rounded">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Modifier Axe</h5>
                <div class="p-4 rounded">
                    <div class="container mt-4">



                        <form method="POST" action="{{ route('Axes.update', $Axes->id) }}">

                            @csrf
                            @method('PUT')
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-1">
                                            <label for="nom_niveau" class="form-label">Nom</label>
                                            <input value="{{ $Axes->nom_axes }}" type="text" class="form-control"
                                                name="nom_axes">
                                        </div>
                                        <div class="mb-1">
                                            <label for="nom_niveau" class="form-label">Description</label>
                                            <input value="{{ $Axes->description_axes }}" type="text" class="form-control"
                                                name="nom_axes">
                                        </div>
                                        <div class="input-group-prepend">
                                            <select class="custom-select" id="isActive" name="isActif">
                                                @if ($Axes->isActif == 'isActif')
                                                    <option selected>{{ $Axes->isActif }}</option>
                                                    <option value='Desactiver'> Desactiver</option>
                                                @else
                                                    <option selected>{{ $Axes->isActif }}</option>
                                                    <option value='Activer'> Activer</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Modifier Niveau</button>
                            <a href="{{ route('Axes.index') }}" class="btn btn-default">Back</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    @endsection
