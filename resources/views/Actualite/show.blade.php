@extends('layouts.app')

@section('title')
Show niveau
@endsection

@section('content')
<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Show user</h5>

            <div class="container mt-4">
                <div>
                    Name: {{ $niveau->nom_niveau }}
                </div>
                <div class="mt-4">
                    {{-- <a href="{{ route('etudiant.edit') }}" class="btn btn-info">Edit</a> --}}
                    <a href="{{ route('niveau.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection
