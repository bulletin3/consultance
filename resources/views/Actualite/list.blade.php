@extends('layouts.app')

@section('title')
Actualite List
@endsection

@section('content')

<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Actualite</h5>
            <h6 class="card-subtitle mb-2 text-muted">Gerer vos Actualite</h6>

            <div class="mt-2">
                @include('layouts.includes.messages')
            </div>
            <div class="mb-2 text-end">
                <a href="{{ route('actualite.create') }}" class="btn btn-info btn-loading" data-coreui-toggle="loading-button ">Ajouter Actualité</a>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" width="1%">#</tpermissionh>
                        <th scope="col" width="15%">Image</th>
                        <th scope="col" width="15%">Titre</th>
                        <th scope="col" width="15%">Mini Description</th>
                        <th scope="col" width="15%">Home</th>
                        <th scope="col" width="15%">Archivre</th>
                        <th scope="col" width="15%">Calendrier</th>
                        <th width="3%" colspan="3">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($actualite as $items)
                    <tr>
                        <th scope="row">{{ $items->id }}</th>
                        <td> <img src="{{Storage::url($items->image_actualite)}}" width="50" height="60"></td>
                        <td>{{ $items->titre_actualite }}</td>
                        <td>{{ $items->mini_description }}</td>
                        <td>
                            @if ($items->Home = 'on')
                                <span>Activer</span>
                                @else
                                <span>Desactiver</span>
                            @endif
                        </td>
                        <td>
                            @if ($items->Archive = 'on')
                                <span>Activer</span>
                                @else
                                <span>Desactiver</span>
                            @endif
                        </td>
                        <td>
                            @if ($items->calendrier = 'on')
                                <span>Activer</span>
                                @else
                                <span>Desactiver</span>
                            @endif
                        </td>
                        <th scope="row">{{ $items->isActif }}</th>
                        <td><a href="{{ route('actualite.show', $items->id) }}" class="btn btn-warning btn-sm">Show</a></td>
                        <td><a href="{{ route('actualite.edit', $items->id) }}" class="btn btn-info btn-sm">Edit</a></td>
                        <td>
                            {!! Form::open(['method' => 'DELETE','route' => ['actualite.destroy',
                            $items->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="d-flex">
                {{-- {!! $users->links() !!} --}}
            </div>

        </div>
    </div>
</div>
@endsection
