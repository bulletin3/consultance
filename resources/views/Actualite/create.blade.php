@extends('layouts.app')

@section('title')
Create Etudiant
@endsection

@section('content')
<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ajouter Nouvelle Actualite</h5>
            <div class="p-4 rounded">
                <div class="container mt-4">



                    <form method="POST" action="{{route('actualite.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="row">
                              <div class="col">
                                <label class="block mb-4">
                                    <span class="sr-only">Choose File</span>
                                    <input type="file" name="image_actualite"/>
                                    @error('image')
                                    <span class="text-red-600 text-sm">{{ $message }}</span>
                                    @enderror
                                </label>
                                <div class="mb-1">
                                    <div class="col-md">
                                        <div class="form-floating">
                                          <select class="form-select" id="floatingSelectGrid" aria-label="Floating label select example" name="axe_id">
                                            <option selected>Choisir Une formation</option>
                                            @foreach ($axes as $items)
                                            <option value=" {{$items->id}} ">{{$items->nom_axes}}</option>
                                            @endforeach

                                          </select>
                                          <label for="floatingSelectGrid">Formation</label>
                                        </div>
                                      </div>
                                </div>
                                <div class="mb-1 ">
                                    <label for="titre_actualite" class="form-label">Titre</label>
                                    <input value="{{ old('titre_actualite') }}" type="text" class="form-control" name="titre_actualite"
                                        required>
                                </div>
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">Mini Description</label>
                                    <textarea value="{{ old('nom_axes') }}"class="form-control" name="mini_description"style="height: 50px"
                                        required></textarea>
                                </div>
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">Description</label>
                                    <textarea value="{{ old('nom_axes') }}"class="form-control" name="description_actualite"style="height: 100px"
                                        required></textarea>
                                </div>

                                <div class="mb-1 ">
                                    <label for="nom_niveau" class="form-label">Professeur</label>
                                    <input value="{{ old('professeur') }}" type="text" class="form-control" name="prof"
                                        required>
                                </div>
                                <div class="mb-1 ">
                                    <label for="datedebut" class="form-label">Date Debut</label>
                                    <input value="{{ old('datedebut') }}" type="date" class="form-control" name="datedebut"
                                        required>
                                </div>
                                <div class="mb-1 ">
                                    <label for="heuredebut" class="form-label">Heure Debut</label>
                                    <input value="{{ old('heuredebut') }}" type="time" class="form-control" name="heuredebut"
                                        required>
                                </div>


                                <div class="mb-1 ">
                                    <label for="datefin" class="form-label">Date Fin</label>
                                    <input value="{{ old('datefin') }}" type="date" class="form-control" name="datefin"
                                        required>
                                </div>
                                <div class="mb-1 ">
                                    <label for="heurefin" class="form-label">Heure Fin</label>
                                    <input value="{{ old('heurefin') }}" type="time" class="form-control" name="heurefin"
                                        required>
                                </div>

                                <div class="m-lg-3">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" data-toggle="toggle" class="form-control" name="Home">
                                      Affiche Sur le menu
                                    </label>
                                  </div>
                                  <br/>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" data-toggle="toggle" class="form-control" name="Archive">
                                      Archiver
                                    </label>
                                  </div>
                                  <br>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" data-toggle="toggle" class="form-control" name="calendrier">
                                      calendrier
                                    </label>
                                  </div>
                                </div>

                                {{-- <div class="mb-1">
                                    <div class="input-group-prepend">
                                      <select class="form-select" id="floatingSelectGrid" aria-label="Floating label select example"id="isActive" name="isActif">
                                        <option selected>Choose...</option>
                                        <option value='Activer'> Activer</option>
                                        <option value='Desactiver'> Desactiver</option>
                                      </select>
                                    </div>
                                </div> --}}
                              </div>
                            </div>
                          </div>

                        <button type="submit" class="btn btn-primary">Enregistrer Niveau</button>
                        <a href="{{ route('actualite.index') }}" class="btn btn-default">Back</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endsection

