@extends('layouts.app')

@section('title')
Create Etudiant
@endsection

@section('content')
<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ajouter Nouvelle niveaus</h5>
            <div class="p-4 rounded">
                <div class="container mt-4">



                    <form method="POST" action="{{route('Client.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="row">
                              <div class="col">
                                <label class="block mb-4">
                                    <span class="sr-only">Choose File</span>
                                    <input type="file" name="image_client"/>
                                    @error('image')
                                    <span class="text-red-600 text-sm">{{ $message }}</span>
                                    @enderror
                                </label>
                                <div class="mb-1">
                                    <div class="input-group-prepend">
                                      <select class="custom-select" id="isActif_client" name="isActif_client">
                                        <option selected>Choose...</option>
                                        <option value='Activer'> Activer</option>
                                        <option value='Desactiver'> Desactiver</option>
                                      </select>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        <button type="submit" class="btn btn-primary">Enregistrer Niveau</button>
                        <a href="{{ route('Client.index') }}" class="btn btn-default">Back</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endsection

