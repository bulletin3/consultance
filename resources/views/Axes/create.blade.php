@extends('layouts.app')

@section('title')
Create Etudiant
@endsection

@section('content')
<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ajouter Nouvelle niveaus</h5>
            <div class="p-4 rounded">
                <div class="container mt-4">



                    <form method="POST" action="{{route('Axes.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="row">
                              <div class="col">
                                <label class="block mb-4">
                                    <span class="sr-only">Choose File</span>
                                    <input type="file" name="image_axes"/>
                                    @error('image')
                                    <span class="text-red-600 text-sm">{{ $message }}</span>
                                    @enderror
                                </label>
                                <div class="mb-1 ">
                                    <label for="nom_niveau" class="form-label">Titre</label>
                                    <input value="{{ old('nom_axes') }}" type="text" class="form-control" name="nom_axes"
                                        required>
                                </div>
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">Description</label>
                                    <textarea value="{{ old('nom_axes') }}"  class="form-control" style="height: 100px" name="description_axes"
                                        required></textarea>
                                </div>




                                <br>
                                <div class="mb-1">
                                    <div class="input-group-prepend">
                                      <select class="custom-select form-control" id="isActive" name="isActif">
                                        <option selected>Choose...</option>
                                        <option value='Activer'> Activer</option>
                                        <option value='Desactiver'> Desactiver</option>
                                      </select>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        <button type="submit" class="btn btn-primary">Enregistrer Niveau</button>
                        <a href="{{ route('Axes.index') }}" class="btn btn-default">Back</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endsection

