@extends('layouts.app')

@section('title')
Modiifier Etudiant
@endsection

@section('content')
<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Modifier Nouvelle niveaus</h5>
            <div class="p-4 rounded">
                <div class="container mt-4">



                    <form method="POST" action="{{ route('niveau.update',$niveau->id) }}">

                        @csrf
        @method('PUT')
                        <div class="container">
                            <div class="row">
                              <div class="col">
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">Nom</label>
                                    <input value="{{ $niveau->nom_niveau }}" type="text" class="form-control" name="nom_niveau"
                                        placeholder="IDA" required>
                                </div>
                              </div>
                            </div>
                          </div>

                        <button type="submit" class="btn btn-primary">Modifier Niveau</button>
                        <a href="{{ route('niveau.index') }}" class="btn btn-default">Back</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endsection

