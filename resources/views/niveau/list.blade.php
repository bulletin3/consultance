@extends('layouts.app')

@section('title')
Filiere List
@endsection

@section('content')

<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Filiere</h5>
            <h6 class="card-subtitle mb-2 text-muted">Gerer vos filieres</h6>

            <div class="mt-2">
                @include('layouts.includes.messages')
            </div>
            <div class="mb-2 text-end">
                <a href="{{ route('Axes.create') }}" class="btn btn-info btn-loading" data-coreui-toggle="loading-button ">Ajouter Axe</a>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" width="1%">#</tpermissionh>
                        <th scope="col" width="15%">Image</th>
                        <th scope="col" width="15%">Titre</th>
                        <th scope="col" width="15%">Description</th>
                        <th width="3%" colspan="3">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Axes as $items)
                    <tr>
                        <th scope="row">{{ $items->id }}</th>
                        <td>{{ $items->image_axes }}</td>
                        <td>{{ $items->nom_axes }}</td>
                        <td>{{ $items->description_axes }}</td>
                        <td><a href="{{ route('Axes.show', $items->id) }}" class="btn btn-warning btn-sm">Show</a></td>
                        <td><a href="{{ route('Axes.edit', $items->id) }}" class="btn btn-info btn-sm">Edit</a></td>
                        <td>
                            {!! Form::open(['method' => 'DELETE','route' => ['Axes.destroy',
                            $items->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="d-flex">
                {{-- {!! $users->links() !!} --}}
            </div>

        </div>
    </div>
</div>
@endsection
