@extends('layouts.app')

@section('title')
Create Etudiant
@endsection

@section('content')
<div class="bg-light rounded">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Ajouter Nouvelle niveaus</h5>
            <div class="p-4 rounded">
                <div class="container mt-4">



                    <form method="POST" action="{{ route('Axes.store') }}">
                        @csrf
                        <div class="container">
                            <div class="row">
                              <div class="col">
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">Nom</label>
                                    <input value="{{ old('nom_axes') }}" type="text" class="form-control" name="nom_niveau"
                                        placeholder="Licence 1" required>
                                </div>
                                <div class="mb-1">
                                    <label for="nom_niveau" class="form-label">Nom</label>
                                    <input value="{{ old('nom_axes') }}" type="text" class="form-control" name="nom_niveau"
                                        placeholder="Licence 1" required>
                                </div>
                              </div>
                            </div>
                          </div>

                        <button type="submit" class="btn btn-primary">Enregistrer Niveau</button>
                        <a href="{{ route('Axes.index') }}" class="btn btn-default">Back</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endsection

