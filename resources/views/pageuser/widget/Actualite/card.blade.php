
@if (!$actualite)
<blockquote >
    <p>
     Desolé Pas d'actualité
    </p>
  </blockquote>
@else
@foreach ($actualite as $items)
<article class="entry">

    <div class="entry-img">
        <img src="{{Storage::url($items->image_actualite)}}" class="img-fluid">
    </div>

    <h2 class="entry-title">
        <a href="blog-single.html"> {{$items->titre}} </a>
    </h2>

    <div class="entry-meta">
        <ul>
            <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a
                    href="blog-single.html">{{$items->prof}}</a></li>
            <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a
                    href="blog-single.html"><time datetime="2020-01-01">{{$items->datedebut}}</time></a></li>

        </ul>
    </div>

    <div class="entry-content">
        <p>
            {{$items->mini_description}}
        </p>
        <div class="read-more">
            <a href="{{url("actualites", $items->id)}}">Read More</a>
        </div>
    </div>

</article>

@endforeach
{!! $actualite->links() !!}
@endif



