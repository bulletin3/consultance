

  <section id="portfolio" class="portfolio">
    <div class="container">

      <div class="row">
        <div class="col-lg-12 d-flex justify-content-center">
          <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">All</li>

          </ul>
        </div>
      </div>

      <div class="row portfolio-container">
        @foreach ($actualite as $items)
        <div class="col-lg-4 col-md-12 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="{{url('storage/'.$items->image)}}" class="img-fluid" alt="" width="300" height="300">
              <div class="portfolio-info">
                <h4> {{$items->titre}} </h4>
                <p>{{$items->mini_description}}</p>
                <div class="portfolio-links">
                  <a href="url(storage/{{$items->image}})" data-gallery="portfolioGallery" class="portfolio-lightbox" title="Management 1"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="portfolio-details-lightbox" data-glightbox="type: external" title="Portfolio Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>

    </div>
  </section>

  {{-- <div class="card" style="width: 18rem;">
    <img src="..." class="card-img-top" alt="...">
    <div class="card-body">
      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    </div>
  </div> --}}





