<section id="services" class="services">
    <div class="container">

      <div class="row">
        @foreach ($Axes as $items)
        <div class="col-md-6">
            <div class="icon-box">

              <i> <img src="{{Storage::url($items->image_axes)}}" width="30" height="30"></i>
              <h4><a href="#">{{$items->nom_axes}}</a></h4>
              <p> {{$items->description_axes}} </p>
            </div>
          </div>
        @endforeach


      </div>

    </div>
  </section>
