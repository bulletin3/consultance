<base href="/public/">
        <nav id="navbar" class="navbar">
            <ul>
                <li class="dropdown"><a href="#"><span>Domaine de formation</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        {{-- {{route('calendriers.index')}} --}}
                        <li><a href="{{ route('nosDomaines') }}">Nos domaines</a></li>
                        <li><a href="{{ route('catalogue') }}">Catalogue</a></li>
                        <li><a href="{{ route('calendriers.index') }}">Calendrier de formation</a></li>

                    </ul>
                </li>

                <li class="dropdown"><a href="#"><span>Accompagnement</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="about.html">Accompagnement à la certification ISO</a></li>
                        <li><a href="team.html">Méthode IFER</a></li>
                        <li><a href="testimonials.html">Gestion de projets</a></li>
                        <li><a href="testimonials.html">Audit</a></li>

                    </ul>
                </li>


                <li class="dropdown"><a href="#"><span>Event</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="about.html">Event Corporate</a></li>
                        <li><a href="team.html">Location de salle</a></li>

                    </ul>
                </li>
                <li class=""><a href="{{ route('actualites.index') }}"><span>Actualité</span> <i
                            class="bi bi-chevron"></i></a>
                    {{-- {{route('actualites')}} --}}
                <li class=""><a href=""><span>À propos de</span> <i class="bi bi-chevron"></i></a>

                <li class=""><a href="{{ route('login') }}"><span>Connexion</span> <i class="bi bi-chevron"></i></a>



            </ul>
            <i class="bi mobile-nav-toggle bi-list"></i>
        </nav><!-- .navbar -->



