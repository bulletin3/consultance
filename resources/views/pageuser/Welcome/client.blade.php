<section id="clients" class="clients section-bg">
    <div class="container">

        <div class="row">

            @foreach ($clients as $items)
                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                    <img src="{{ Storage::url($items->image_client) }}" class="img-fluid" alt="">
                </div>
            @endforeach

        </div>

    </div>
</section>
