@include('pageuser.Menu.header')

<base href="/public">
{{-- <main id="main">


    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">

                <h2>
                    @foreach ($actualite as $items)
                        {{ $items->titre ?? null }}
                    @endforeach
                </h2>
                <ol>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="blog.html">Actualite</a></li>
                    <li>
                        @foreach ($actualite as $items)
                            {{ $items->titre ?? null }}
                        @endforeach
                    </li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Single Section ======= -->
    <section id="blog" class="blog">
        <div class="container" data-aos="fade-up">

            <div class="row">

                <div class="col-lg-8 entries">

                    <article class="entry entry-single">

                        @foreach ($actualite as $items)
                            <div class="entry-img">
                                <img src="{{ Storage::url($items->image_actualite) }}" class="img-fluid">
                            </div>
                        @endforeach

                        <h2 class="entry-title">
                            <a href="blog-single.html">
                                @foreach ($actualite as $items)
                                    {{ $items->titre ?? null }}
                                @endforeach
                            </a>
                        </h2>

                        <div class="entry-meta">
                            <ul>
                                <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a
                                        href="blog-single.html">
                                        @foreach ($actualite as $items)
                                            {{ $items->prof ?? null }}
                                        @endforeach
                                    </a>
                                </li>
                                <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a
                                        href="blog-single.html"><time datetime="2020-01-01">
                                            @foreach ($actualite as $items)
                                                {{ $items->datedebut ?? null }}
                                            @endforeach
                                        </time></a></li>

                            </ul>
                        </div>

                        <div class="entry-content">
                            <p>
                                @foreach ($actualite as $items)
                                    {{ $items->description ?? null }}
                                @endforeach
                            </p>



                        </div>

                        <div class="entry-footer">
                            <i class="bi bi-folder"></i>
                            <ul class="cats">
                                <li><a href="#">Business</a></li>
                            </ul>

                            <i class="bi bi-tags"></i>
                            <ul class="tags">
                                <li><a href="#">Creative</a></li>
                                <li><a href="#">Tips</a></li>
                                <li><a href="#">Marketing</a></li>
                            </ul>
                        </div>

                    </article><!-- End blog entry -->

                    <!-- End blog author bio -->

                    <!-- End blog comments -->

                </div><!-- End blog entries list -->


            </div>
    </section><!-- End Blog Single Section -->

</main><!-- End #main -->

@include('pageuser.Menu.footer') --}}






<!DOCTYPE html>
<html lang="en">

<!-- End Header -->

@include('pageuser.Menu.header')
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>
            @foreach ($actualite as $items)
            {{$items->titre_actualite}}
            @endforeach
          </h2>
          <ol>
            <li><a href="index.html">Home</a></li>
            <li><a href="blog.html">Actualite</a></li>
            {{-- <li>Blog Single</li> --}}
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Single Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-8 entries">

            <article class="entry entry-single">

                @foreach ($actualite as $items)
                <div class="entry-img">
                    <img src="{{ Storage::url($items->image_actualite) }}" class="img-fluid">
                </div>
            @endforeach

              <h2 class="entry-title">
                  @foreach ($actualite as $items)
                    {{$items->titre_actualite}}
              @endforeach
              </h2>

              <div class="entry-meta">
                <ul>
                    @foreach ($actualite as $items)
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-single.html">{{$items->prof}}</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">{{$items->datedebut}}</time></a></li>
                  @endforeach
                </ul>
              </div>

              <div class="entry-content">
                <p>
                    @foreach ($actualite as $items)
                    {{$items->description_actualite}}
                    @endforeach
                </p>



              </div>

              <div class="entry-footer">
                <i class="bi bi-folder"></i>
                <ul class="cats">
                    @foreach ($actualite as $items)
                    <li>{{$items->Lieu}}</li>
                    @endforeach

                </ul>

                <i class="bi bi-tags"></i>
                <ul class="tags">
                  <li><a href="#">Creative</a></li>
                  <li><a href="#">Tips</a></li>
                  <li><a href="#">Marketing</a></li>
                </ul>
              </div>

            </article><!-- End blog entry -->

           <!-- End blog author bio -->

            <!-- End blog comments -->

          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar">

             @include('pageuser.widget.Actualite.seach')
                <!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div>

      </div>
    </section><!-- End Blog Single Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  @include('pageuser.Menu.footer')
</body>

</html>
